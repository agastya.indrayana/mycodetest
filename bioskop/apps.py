from django.apps import AppConfig


class BioskopConfig(AppConfig):
    name = 'bioskop'
