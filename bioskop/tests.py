from django.test import TestCase, Client
from django.urls import resolve
from . import views, models
import datetime
from django.utils.timezone import make_aware

class HomepageTest(TestCase):
    def test_home_url_exists(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)

    def test_home_using_index_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'bioskop/homepage.html')

    def test_home_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, views.index)

class MovieAPITest(TestCase):
    def test_data_insertion(self):
        #create new instance of Movie
        new_movie = models.Movie.objects.create(name="Avengers: Endgame", duration=183, rating="SU", synopsis="This is a test", type="Regular")

        #Retrieving all available Movie
        movieCount = models.Movie.objects.all().count()
        self.assertEqual(movieCount,1)

    def test_post_request_save_data(self):
        data = {"name": "Avengers: Endgame",
    "duration": 183,
    "rating": "SU",
    "synopsis": "Testing.",
    "type": "Reguler"}
        response = self.client.post('/api/movie/', data=data)
        dataCount = models.Movie.objects.all().count()
        self.assertEqual(dataCount, 1)

        self.assertEqual(response.status_code, 201)

        new_response = self.client.get('/api/movie/')
        html_response = new_response.content.decode('utf8')
        self.assertIn('Avengers: Endgame', html_response)

class StudioAPITest(TestCase):
    def test_data_insertion(self):
        #create new instance of Movie
        new_data = models.Studio.objects.create(name="Studio 1", capacity=150)

        #Retrieving all available Movie
        dataCount = models.Studio.objects.all().count()
        self.assertEqual(dataCount,1)

    def test_post_request_save_data(self):
        data = {"name": "Studio 1",
    "capacity": "150"}
        response = self.client.post('/api/studio/', data=data)
        dataCount = models.Studio.objects.all().count()
        self.assertEqual(dataCount, 1)

        self.assertEqual(response.status_code, 201)

        new_response = self.client.get('/api/studio/')
        html_response = new_response.content.decode('utf8')
        self.assertIn('Studio 1', html_response)

class StudioTypeAPITest(TestCase):
    def test_data_insertion(self):
        studioObj = models.Studio.objects.create(name="Studio 1", capacity=150)
        #create new instance of Movie
        new_data = models.StudioType.objects.create(studio_name=studioObj , type="Regular")

        #Retrieving all available Movie
        dataCount = models.StudioType.objects.all().count()
        self.assertEqual(dataCount,1)

    def test_post_request_save_data(self):
        models.Studio.objects.create(name="Studio 1", capacity=150)
        data = {"studio_name": "Studio 1",
    "type": "Regular"}
        response = self.client.post('/api/studio-type/', data=data)
        dataCount = models.StudioType.objects.all().count()
        self.assertEqual(dataCount, 1)

        self.assertEqual(response.status_code, 201)

        new_response = self.client.get('/api/studio-type/')
        html_response = new_response.content.decode('utf8')
        self.assertIn('Studio 1', html_response)

class ScheduleAPITest(TestCase):
    def test_data_insertion(self):
        movieObject = models.Movie.objects.create(name="Avengers: Endgame", duration=183, rating="SU", synopsis="This is a test", type="Regular")
        studioObject = models.Studio.objects.create(name="Studio 1", capacity=150)
        start_datetime =  make_aware(datetime.datetime.now())
        end_datetime = make_aware(datetime.datetime.now() + datetime.timedelta(hours = 3))
        #create new instance of Movie
        new_data = models.Schedule.objects.create(movie=movieObject,studio=studioObject,start_datetime = start_datetime, end_datetime = end_datetime)

        #Retrieving all available Movie
        dataCount = models.Schedule.objects.all().count()
        self.assertEqual(dataCount,1)

    def test_post_request_save_data(self):
        movieObject = models.Movie.objects.create(name="Avengers: Endgame", duration=183, rating="SU", synopsis="This is a test", type="Regular")
        studioObject = models.Studio.objects.create(name="Studio 1", capacity=150)
        start_datetime =  make_aware(datetime.datetime.now())
        end_datetime = make_aware(datetime.datetime.now() + datetime.timedelta(hours = 3))
        data = {"movie": "Avengers: Endgame",
    "studio": "Studio 1",
    "start_datetime": start_datetime,
    "end_datetime": end_datetime}
        response = self.client.post('/api/schedule/', data=data)
        dataCount = models.Schedule.objects.all().count()
        self.assertEqual(dataCount, 1)

        self.assertEqual(response.status_code, 201)

        new_response = self.client.get('/api/schedule/')
        html_response = new_response.content.decode('utf8')
        self.assertIn('Studio 1', html_response)

class CustomerAPITest(TestCase):
    def test_data_insertion(self):
        #create new instance of Movie
        new_data = models.Customer.objects.create(email="agas@exmaple.com",name="agastya",birth_date = datetime.date(1999, 6, 2), sex = "male")

        #Retrieving all available Movie
        dataCount = models.Customer.objects.all().count()
        self.assertEqual(dataCount,1)

    def test_post_request_save_data(self):
        data = {"email": "agas@example.com",
        "name": "agastya",
        "birth_date": str(datetime.date(1999, 6, 2)),
        "sex": "male"}
        response = self.client.post('/api/customer/', data=data)
        dataCount = models.Customer.objects.all().count()
        self.assertEqual(dataCount, 1)

        self.assertEqual(response.status_code, 201)

        new_response = self.client.get('/api/customer/')
        html_response = new_response.content.decode('utf8')
        self.assertIn("agas@example.com", html_response)

class EmployeeAPITest(TestCase):
    def test_data_insertion(self):
        #create new instance of Movie
        new_data = models.Employee.objects.create(NIP="EMP00001",name="agastya",email="agas@exmaple.com",birth_date = datetime.date(1999, 6, 2), sex = "male",
        phone_number = "0815-555-753", address="Kali Baru, Cilincing, Kota Jakarta Utara, Dki Jakarta")

        #Retrieving all available Movie
        dataCount = models.Employee.objects.all().count()
        self.assertEqual(dataCount,1)

    def test_post_request_save_data(self):
        data = {"NIP":"EMP00001", "name":"agastya", "email":"agas@example.com", "birth_date" : datetime.date(1999, 6, 2), "sex" : "male", "phone_number" : "0815-555-753", "address":"Kali Baru, Cilincing, Kota Jakarta Utara, Dki Jakarta"}
        response = self.client.post('/api/employee/', data=data)
        dataCount = models.Employee.objects.all().count()
        self.assertEqual(dataCount, 1)

        self.assertEqual(response.status_code, 201)

        new_response = self.client.get('/api/employee/')
        html_response = new_response.content.decode('utf8')
        self.assertIn("agas@example.com", html_response)

class TransactionAPITest(TestCase):
    def test_data_insertion(self):
        customerObj = models.Customer.objects.create(email="mika@exmaple.com",name="mika",birth_date = datetime.date(1999, 6, 2), sex = "male")
        employeeObj = models.Employee.objects.create(NIP="EMP00001",name="agastya",email="agas@exmaple.com",birth_date = datetime.date(1999, 6, 2), sex = "male",
        phone_number = "0815-555-753", address="Kali Baru, Cilincing, Kota Jakarta Utara, Dki Jakarta")
        #create new instance of Movie
        new_data = models.Transaction.objects.create(customer_email = customerObj, employee_nip = employeeObj, total = 50000)

        #Retrieving all available Movie
        dataCount = models.Transaction.objects.all().count()
        self.assertEqual(dataCount,1)

    def test_post_request_save_data(self):
        models.Customer.objects.create(email="mika@example.com",name="mika",birth_date = datetime.date(1999, 6, 2), sex = "male")
        models.Employee.objects.create(NIP="EMP00001",name="agastya",email="agas@exmaple.com",birth_date = datetime.date(1999, 6, 2), sex = "male",
        phone_number = "0815-555-753", address="Kali Baru, Cilincing, Kota Jakarta Utara, Dki Jakarta")

        data = {"customer_email" : "mika@example.com", "employee_nip":"EMP00001", "total" : 50000}
        response = self.client.post('/api/transaction/', data=data)
        dataCount = models.Transaction.objects.all().count()
        self.assertEqual(dataCount, 1)

        self.assertEqual(response.status_code, 201)

        new_response = self.client.get('/api/transaction/')
        html_response = new_response.content.decode('utf8')
        self.assertIn("mika@example.com", html_response)
