from django.db import models

class Movie(models.Model):
    name = models.CharField(max_length=50, unique=True)
    duration = models.IntegerField()
    rating = models.CharField(max_length=10)
    synopsis = models.TextField()
    type = models.CharField(max_length=10)

class Studio(models.Model):
    name =  models.CharField(max_length=50, unique=True)
    capacity = models.IntegerField()

class StudioType(models.Model):
    studio_name = models.ForeignKey(Studio, on_delete=models.CASCADE, to_field="name")
    type = models.CharField(max_length=10)

class Schedule(models.Model):
    movie = models.ForeignKey(Movie, on_delete=models.CASCADE, to_field='name')
    studio = models.ForeignKey(Studio, on_delete=models.CASCADE, to_field='name')
    start_datetime = models.DateTimeField()
    end_datetime = models.DateTimeField()

class Customer(models.Model):
    email = models.EmailField(unique=True)
    name = models.CharField(max_length = 50)
    birth_date = models.DateField()
    sex = models.CharField(max_length=20)

class Employee(models.Model):
    NIP = models.CharField(max_length=8, unique=True)
    name = models.CharField(max_length = 50)
    email = models.EmailField(unique=True)
    birth_date = models.DateField()
    sex = models.CharField(max_length=20)
    phone_number = models.CharField(max_length = 16)
    address = models.CharField(max_length = 100)

class Transaction(models.Model):
    timestamp = models.DateTimeField(auto_now_add=True, unique=True)
    customer_email = models.ForeignKey(Customer, on_delete=models.CASCADE, to_field="email")
    employee_nip = models.ForeignKey(Employee, on_delete=models.CASCADE, to_field="NIP")
    total = models.PositiveIntegerField()
