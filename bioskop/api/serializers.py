from rest_framework import serializers
from bioskop.models import Movie, Studio, StudioType, Schedule, Customer, Employee, Transaction

class MovieSerializer(serializers.ModelSerializer):
    class Meta:
        model = Movie
        fields = ('id', 'name', 'duration','rating', 'synopsis', 'type')

class StudioSerializer(serializers.ModelSerializer):
    class Meta:
        model = Studio
        fields = ('id', 'name', 'capacity')

class StudioTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = StudioType
        fields = ('id', 'studio_name', 'type')

class ScheduleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Schedule
        fields = ('id', 'movie', 'studio', 'start_datetime', 'end_datetime')

class CustomerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Customer
        fields = ('id', 'email', 'name', 'birth_date', 'sex')

class EmployeeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Employee
        fields = ('id','NIP', 'email', 'name', 'birth_date', 'sex','phone_number','address')

class TransactionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Transaction
        fields = ('id', 'timestamp', 'customer_email', 'employee_nip', 'total')
