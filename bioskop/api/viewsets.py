from bioskop.models import Movie, Studio, StudioType, Schedule, Customer,Employee, Transaction
from .serializers import MovieSerializer, StudioSerializer, StudioTypeSerializer, ScheduleSerializer, CustomerSerializer, EmployeeSerializer, TransactionSerializer
from rest_framework import viewsets

class MovieViewSet(viewsets.ModelViewSet):
    queryset = Movie.objects.all()
    serializer_class = MovieSerializer

class StudioViewSet(viewsets.ModelViewSet):
    queryset = Studio.objects.all()
    serializer_class = StudioSerializer

class StudioTypeViewSet(viewsets.ModelViewSet):
    queryset = StudioType.objects.all()
    serializer_class = StudioTypeSerializer

class ScheduleViewSet(viewsets.ModelViewSet):
    queryset = Schedule.objects.all()
    serializer_class = ScheduleSerializer

class CustomerViewSet(viewsets.ModelViewSet):
    queryset = Customer.objects.all()
    serializer_class = CustomerSerializer

class EmployeeViewSet(viewsets.ModelViewSet):
    queryset = Employee.objects.all()
    serializer_class = EmployeeSerializer

class TransactionViewSet(viewsets.ModelViewSet):
    queryset = Transaction.objects.all()
    serializer_class = TransactionSerializer
