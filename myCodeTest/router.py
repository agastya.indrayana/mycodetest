from bioskop.api.viewsets import MovieViewSet, StudioViewSet, StudioTypeViewSet, ScheduleViewSet,CustomerViewSet, EmployeeViewSet, TransactionViewSet
from rest_framework import routers

router = routers.DefaultRouter()
router.register('movie', MovieViewSet)
router.register('studio', StudioViewSet)
router.register('studio-type', StudioTypeViewSet)
router.register('schedule', ScheduleViewSet)
router.register('customer', CustomerViewSet)
router.register('employee', EmployeeViewSet)
router.register('transaction', TransactionViewSet)
